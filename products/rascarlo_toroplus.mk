# Inherit AOSP device configuration for toroplus.
$(call inherit-product, device/samsung/toroplus/full_toroplus.mk)

# Inherit common product files.
$(call inherit-product, vendor/rascarlo/products/common.mk)
 
# Setup device specific product configuration.
PRODUCT_NAME := rascarlo_toroplus
PRODUCT_BRAND := google
PRODUCT_DEVICE := toroplus
PRODUCT_MODEL := Galaxy Nexus
PRODUCT_MANUFACTURER := samsung

PRODUCT_BUILD_PROP_OVERRIDES += PRODUCT_NAME=mysidspr BUILD_DISPLAY_ID=JDQ39 BUILD_FINGERPRINT="samsung/mysidspr/toroplus:4.2.2/JDQ39/L700GA02:user/release-keys" PRIVATE_BUILD_DESC="mysidspr-user 4.2.2 JDQ39 L700GA02 release-keys"

# Inherit media effect blobs
-include vendor/rascarlo/products/common_media_effects.mk

# Inherit common build.prop overrides
-include vendor/rascarlo/products/common_versions.mk
